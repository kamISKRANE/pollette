<!DOCTYPE html>

<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
</head>
<body>
<div class="wrap">
	<div id="icon-options-general" class="icon32"><br /></div>
	<h1>Mes options</h1>
	<br>
	<form method="post" action="">
		<fieldset id="field4">
			<legend id="titlesondage">Traductions</legend>
			<p>
				<label for="simple-rate-rate_button">Bouton de vote:</label>
				<input id="simple-rate" type="text" name="rate_button" value="<?php echo $rate_button ?>" />
			</p>
			<p>
				<label for="simple-rate-already_rated">Sondage déjà voté:</label>
				<input id="simple-rate" type="text" name="already_rated" value="<?php echo $already_rated ?>" />
			</p>
			<p>
				<label for="simple-rate-thankyou">Remerciement:</label>
				<input id="simple-rate" type="text" name="thankyou" value="<?php echo $thankyou ?>" />
			</p>
		</fieldset>
		<br>
		<fieldset id="field4">
			<legend id="titlesondage">Résultats</legend>
			<p>
				<label for="simple-rate-poll_results_label">Titre résultats de sondage:</label>
				<input id="simple-rate" type="text" name="poll_results_label" value="<?php echo $poll_results_label ?>" />
			</p>
			<p>
				<label for="simple-rate-question_label">Titre questions:</label>
				<input id="simple-rate" type="text" name="question_label" value="<?php echo $question_label ?>" />
			</p>
			<p>
				<label for="simple-rate-answers_label">Titre réponses:</label>
				<input id="simple-rate" type="text" name="answers_label" value="<?php echo $answers_label ?>" />
			</p>
			<p>
				<label for="simple-rate-answer_structure">Texte des statistiques:</label>
				<input id="simple-rate" type="text" name="answer_structure" value="<?php echo $answer_structure?>" />
			</p>
			<p>
				<label for="simple-rate-most_rated">Titre des réponses les plus votés:</label>
				<input id="simple-rate" type="text" name="most_rated" value="<?php echo $most_rated ?>" />
			</p>
			
		</fieldset>
		<br>
		<fieldset id="field4">
			<legend id="titlesondage">Options</legend>
			<p>
				<input type="radio" <?php echo ($show_who_rates=="y")?'checked="checked"':''?> name="show_who_rates" id="show_who_rates_y" value="y" />
				<label for="show_who_rates_y" >Afficher les utilisateurs ayants votés (dans le panel admin)</label>
			</p>
			<p>
				<input type="radio" <?php echo ($show_who_rates=="n")?'checked="checked"':''?> name="show_who_rates" id="show_who_rates_n" value="n" />
				<label for="show_who_rates_n">Ne pas afficher les utilisateurs ayants votés (dans le panel admin)</label>
			</p>
		<p><input type="submit" id="button-toto" value="Enregistrer" /></p>
	</form>
</div>

</body>