<!DOCTYPE html>

<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
</head>
<div class="wrap">
	<div id="icon-options-general" class="icon32"><br /></div>
	<h1>Pollette - Sondages</h1>
	<br>
	<table class="widefat">
		<tr>
			<th><label for="sp-question" id="title2">Shortcode</label></th>
			<th><label for="sp-question" id="title2">Question</label></th>
			<th><label for="sp-question" id="title2">Nombre de réponses</label></th>
			<th><label for="sp-question" id="title2">Expiration</label></th>
			<th><label for="sp-question" id="title2">Action</label></th>
		</tr>

		<tbody>
			<?php
			
			foreach($results as $row){
				$sql = "select count(id) from $tableRates where poll_id = $row->id";
				$rating = $wpdb->get_var($sql, 0,0); 
				?>
				<tr <?php echo ($row->since > date('Y-m-d H:i:s'))?'class="expired"':'' ?>>
					<td><code>[pollette id="<?php echo $row->id ?>"]</code></td>
					<td><a href="?<?php echo $_SERVER['QUERY_STRING'] ?>&id=<?php echo $row->id ?>"><?php echo($row->question); ?></a></td>
					<td>
						
						<label for="sp-question" id="title3"><?php echo($rating); ?></label>
					</td>
					<td>
						<?php echo ($row->since<date('Y-m-d H:i:s'))?'(expired)' : date('d/m/Y', strtotime($row->since)) ?>
					</td>
					<td><a href="?<?php echo $_SERVER['QUERY_STRING'] ?>&delid=<?php echo $row->id ?>"> Supprimer </a></td>
				</tr>
				<?php
			}
			?>
		</tbody>
	</table>
	<br>
		<p><a id="new" href="?<?php echo $_SERVER['QUERY_STRING'] ?>&id=0">Nouveau sondage</a></p>
</div>