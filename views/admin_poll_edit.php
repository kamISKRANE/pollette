<!DOCTYPE html>

<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
</head>

<div class="wrap">
	<div id="icon-options-general" class="icon32"><br /></div>
	<h1>Pollette - Sondages</h1><br>
	<form method="post" action="">
		<table id="tata">
			<tr>
				<th>
					<label for="sp-question" id="title">Question:</label>
				</th>
				<td>
					<input type="text" name="question" id="simple-rate" value="<?php echo htmlspecialchars($row->question) ?>" />
				</td>
			</tr>
			<tr>
				<th>
					<label for="sp-since" id="title">Date d'expiration:</label>
				</th>
				
				<td>
					<input type="date" name="since" id="simple-rate" min="<?=date('Y-m-d', strtotime('+ 1 days'))?>" value="<?php echo $row->since ?>" required />
				</td>
			</tr>
			<?php 
			if($rates==0){
				?>
				<tr>
					<th>
						<label for="sp-answers" id="title">Réponses:</label>
					</th>
					<td>
						<textarea rows="6" cols="80" name="answers" id="simple-rate"><?php echo htmlspecialchars($row->answers) ?></textarea>
						<p class="help">
							Indiquez une réponse par ligne.
						</p>
					</td>
				</tr>
				<?php 
			}else{
				?>
				<tr>
					<th>
						<label for="sp-since" id="title">Réponses:</label>
					</th>
					<td>
						<?php echo nl2br($row->answers) ?>
						<p class="help">
							Vous ne pouvez pas modifier les réponses lorsqu'un utilisateur à déja voté au sondage.
						</p>
					</td>
				</tr>
			<?php } ?>
		</table>
		<p>
			<p><input type="submit" id="button-toto" value="Valider" /></p>

		
		</p>
	</form>
	
	<?php if($row->id): ?>
		<?php 
			$options = self::loadOptions();
			
			if($options['show_who_rates']=='y'){
				?>
				<tr>
					<th>
						<strong>Les utilisateurs suivants ont votés:</strong>
					</th>
					<td>
						<?php
						$sql = "select user_id from $tableRates where poll_id = {$row->id}";
						$subRows = $wpdb->get_results($sql);
						foreach($subRows as $user){
							$userInfo = get_user_by('id', $user->user_id);
							echo '<span title="' . htmlspecialchars( $userInfo->display_name) .'">';
							echo get_avatar($userInfo->ID, 64,'hello', $userInfo->display_name);
							echo "</span>"; 
						}
						?><br />
					</td>
				</tr>
				<?php
			} 
		?>
		<ul>
			<h3>Statistiques du sondage</h3>
			<?php 
			$answers = preg_split("/\n/", $row->answers);
			$sql_rates = "select answer_index, count(answer_index) as voti from $tableRates where poll_id = {$row->id} group by answer_index";
			$results = $wpdb->get_results($sql_rates);
			foreach($results as $key => $subRow){
				?>
				<li>
					<b><?=$answers[$subRow->answer_index] ?> :</b> <?php echo $subRow->voti ?> votes
				</li>
				<?php
			}
			?>
		</ul>
	<?php endif; ?>
	
</div>
<script
	src="https://code.jquery.com/jquery-3.4.1.js"
	integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
	crossorigin="anonymous">
</script>
<script type="text/javascript">
$(document).ready(function(){
	var rep_textarea = '<textarea rows="6" cols="80" name="answers" id="sp-answers"><?php echo htmlspecialchars($row->answers) ?></textarea>\
		<p class="help">Indiquez une réponse par ligne.</p>';
	var rep_checkbox = '<input type="button" value="Ajouter une réponse">';
	$('input[name=type_reponse]').change(function(){
		if($('input[name=type_reponse]:checked').val() == "textarea"){
			$("#reponse").html(rep_textarea);
		}
		else if($('input[name=type_reponse]:checked').val() == "checkbox"){
			$("#reponse").html(rep_checkbox);
		}
		console.log($('input[name=type_reponse]:checked').val());	
	});
});
</script>