<?php 


// Creating the widget 
class wpb_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'wpb_widget', 

			// Widget name will appear in UI
			__('Pollette Widget', 'wpb_widget_domain'), 

			// Widget description
			array( 'description' => __( 'Widget to display selected poll. ', 'wpb_widget_domain' ), ) 
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		$poll_id = $instance['poll_id'];
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $poll_id ) ){
			echo "<div>".toPollette::shortcode(['id'=>$poll_id])."</div>";
		}
		echo $args['after_widget'];
	}
	 
	// Widget Backend 
	public function form( $instance ) {	
		global $wpdb;
	
		$title = ( isset( $instance[ 'poll_id' ] ) ) ? $instance[ 'poll_id' ] : 0;
		// Widget admin form
		$polls = $wpdb->prefix.'pollette_polls';
		$sql = "select * from $polls";
		$rows = $wpdb->get_results($sql);?>
		<p>
		<label for="<?php echo $this->get_field_id( 'poll' ); ?>"><?php _e( 'Poll:' ); ?></label> 
		<select id = "poll_select" name = "pollette_widget_select" class = "widefat">
				<?php
				if ( 0 < count( $rows ) ) {
					foreach( $rows as $row ) {
						?>
						<option value="<?php echo $row->id;?>" <?php selected( $row->id, $poll_id ); ?>>
							<?php echo $row->question;?>
						</option>
						<?php
					}
				}
				?>
		</select>
		</p>
		<?php 
	}
	 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
	$instance = array();
	$instance['poll_id'] = ( ! empty( $new_instance['poll_id'] ) ) ? strip_tags( $new_instance['poll_id'] ) : $_POST['pollette_widget_select'];
	
	return $instance;
	}
} // Class wpb_widget ends here