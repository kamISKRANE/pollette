<?php
/*
Plugin Name: Pollette
Plugin URI: http://esgi.com/
Description: Creer des polls simple
Author: Kilian, Kamel, Toan, Saliou
Version: 1.1.1
*/

// Security issue: you cannot run the script invoking it directly
if(__FILE__ == $_SERVER['SCRIPT_FILENAME']) die(); 

include_once 'inc/pollette_widget.php';

register_activation_hook(__FILE__, 	array('toPollette','register'));
add_action('admin_menu', 			array('toPollette','init'));
add_shortcode('pollette', 		array('toPollette','shortcode'));

// Register and load the widget
function wpb_load_widget() {
 register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

/**
 * Register with hook 'wp_enqueue_scripts', which can be used for front end CSS and JavaScript
 */
add_action( 'wp_enqueue_scripts', 'prefix_add_my_stylesheet' );

/**
 * Enqueue plugin style-file
 */
function prefix_add_my_stylesheet() {
    // Respects SSL, Style.css is relative to the current file
    wp_register_style( 'prefix-style', plugins_url('style.css', __FILE__) );
    wp_enqueue_style( 'prefix-style' );
}

class toPollette{
	const POLLS = 'pollette_polls';
	const RATES = 'pollette_rates';
	
	static function init(){
		/* Add menus admin */
		add_menu_page('Pollette', 'Pollette', 'manage_options', 'pollette_menu', array(__CLASS__, 'backend'));
		add_submenu_page('pollette_menu', "Pollette Sondages", "Sondages", 'manage_options', "pollette_polls", array(__CLASS__, 'backend'));
		add_submenu_page('pollette_menu', "Pollette Options", "Options", 'manage_options', "pollette_options", array(__CLASS__, 'backendOptions'));
		remove_submenu_page('pollette_menu','pollette_menu');
	}
	
	static function register(){
		global $wpdb;
		$prefix = $wpdb->prefix;
		
		$polls = $prefix.self::POLLS;
		$rates = $prefix.self::RATES;
		$sql = "
		CREATE TABLE $polls(
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			question longtext NOT NULL,
			answers longtext NOT NULL,
			since datetime,
			PRIMARY KEY  (id)
		);
		
		CREATE TABLE $rates(
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			user_id mediumint(9),
			poll_id mediumint(9),
			expiration_date datetime,
			answer_index mediumint(9),
			PRIMARY KEY  (id)
	
		);
		";
		
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		$wpdb->show_errors(true);
		dbDelta($sql, true);
	}
	static function loadOptions(){
		$options = get_option('polletteOptions');
		if(!is_array($options)) $options = array();
		!isset($options['rate_button']) && $options['rate_button']					= 'Voter!';
		!isset($options['already_rated']) && $options['already_rated'] 				= 'Vous avez déjà voter à ce sondage.';
		!isset($options['thankyou']) && $options['thankyou'] 						= 'Merci, votre vote est pris en compte!';
		!isset($options['poll_results_label']) && $options['poll_results_label'] 	= 'Résultats du sondage';
		!isset($options['question_label']) && $options['question_label'] 			= 'Questions';
		!isset($options['answers_label']) && $options['answers_label'] 				= 'Réponses';
		!isset($options['answer_structure']) && $options['answer_structure'] 		= '%s avec %d des votes';
		!isset($options['most_rated']) && $options['most_rated']  					= 'Les plus votés';
		!isset($options['show_who_rates']) && $options['show_who_rates']			= 'n';
		return $options;
	}
	
	static function saveOptions(){
		$options = self::loadOptions();
		if(isset($_POST) && count($_POST)>0){
			$_POST = stripslashes_deep($_POST);
			$optionsUpdated = false;
			foreach($options as $key => $value){
				
				if(isset($_POST[$key]) && $_POST[$key] != $options[$key]){
					$options[$key] = $_POST[$key];
					$optionsUpdated = true;
				}
				
			}
			if($optionsUpdated) update_option('polletteOptions', $options);
		}
	}
	
	static function backendOptions(){
		self::saveOptions();
		$options = self::loadOptions();
		foreach($options as $key => $value){
			$options[$key] = htmlspecialchars($value);
		}
		extract($options);
		
		include 'views/admin_options.php';		
	}
	static function backend(){
		global $wpdb;
		
		$polls = $wpdb->prefix.self::POLLS;
		$tableRates = $wpdb->prefix.self::RATES;
		if(isset($_POST) && count($_POST)>0){
			$_POST = stripslashes_deep($_POST);
			$wpdb->show_errors(true);
			if($_GET['id']=='0'){
			
				$wpdb->insert($polls, $_POST);
			}else{
				$wpdb->update($polls, $_POST, array('id'=> $_GET['id']));
			}
			
			unset($_GET['id']);
		}
	
		if(isset($_GET['id']) && is_numeric($_GET['id'])){
			$id = $_GET['id'];
			$sql = "select * from $polls where id=$id";
			
			$row = $wpdb->get_row($sql);
			$sql ="select count(id) voti from $tableRates where poll_id=$id";
			$rates = $wpdb->get_var($sql, 0,0);
			
			include 'views/admin_poll_edit.php';
		}elseif (isset($_GET['delid']) && is_numeric($_GET['delid'])) {
			$id = $_GET['delid'];
			$sql = "delete from $polls where id=$id";
			$wpdb->query($sql);
			$sql = "delete from $tableRates where poll_id=$id";
			$wpdb->query($sql);
			include 'views/admin_polls.php';

			
		

		}else{
			# $sql ="select p.id, p.question, count(r.id) voti from $polls p left join $rates r on p.id = r.poll_id order by expiration_date desc";
			$sql ="select * from $polls p order by since desc";
			$results = $wpdb->get_results($sql);
			include 'views/admin_polls.php';	
		}
	}
	
	static function shortcode($arguments){

		$options = self::loadOptions();
		extract($options);
		
		$id = $arguments['id'];
		$uid = get_current_user_id();
		if($uid!=0){
			global $wpdb;
			$polls = $wpdb->prefix.self::POLLS;
			$rates = $wpdb->prefix.self::RATES;
			$sql = "select count(*) from $rates where user_id='$uid' and poll_id='$id'";
			$count = $wpdb->get_var($sql, 0,0);
			$sql = "select * from $polls where id='$id'";
			$data = $wpdb->get_row($sql);
			if($data->since> date('Y-m-d')){
				if($count=='0'){
					
					if(isset($_POST) 
							&& count($_POST)>0 
							&& isset($_POST['answer']) 
							&& is_numeric($_POST['answer'])
							&& isset($_POST['poll'])
							&& is_numeric($_POST['poll'])
							&& $_POST['poll'] == $id
						){
						$answer = $_POST['answer'];
						
						$data= array(
							'user_id'		=> $uid,
							'poll_id'		=> $id,
							'answer_index'	=> $answer,
							'expiration_date' => date('Y-m-d H:i:s')
						);
						if($wpdb->insert($rates, $data)){
							
							$buffer = $thankyou;	
						}
						return $buffer;
					}else{
						$output = '
							<form method="post" action="" id="simple-poll-%d" class="simple-poll">
								<fieldset id="field4">
									<legend id="titlesondage">Sondage</legend>
									
									<h2> %s </h2>
									%s
								<p>
									<input type="hidden" name="poll" value="%1$d" />
									<input type="submit"id="button-toto" value="%s" />
									</p>
								</fieldset>
							
							</form>
						';
						$options = preg_split("/\n/",$data->answers);
						
						$optionList = '';
						foreach($options as $index => $option){
							$optionList .="<p>";
							$optionList .= '<input type="radio" name="answer" value="' . $index .'" id="option-'.$index.'" />';
							$optionList .= "<label for=\"option-$index\">$option</label>";
							$optionList .="</p>";
							
						}
						
						return sprintf($output, $data->id, $data->question, $optionList, $rate_button);
					}
				}else{
					return "<h3>$data->question</h3><p>$already_rated</p>";
				}
			}else{
				
				$sql = "select p.id, p.question, p.answers, count(r.id) rating, r.answer_index idx from $polls p left join $rates r on p.id = r.poll_id where p.id=$id group by r.answer_index order by answer_index asc";
				$results = $wpdb->get_results($sql);
				$options = preg_split("/\n/",$data->answers);
				#print_r($results);
				$rates = array();
				for($i = 0; $i<count($results) || $i<count($options); $i++){
					$data = $results[$i];
					
					if(!isset($rates[$i])) 
						$rates[$i] = array('answer' => $options[$i], 'rates' => 0);
					if(isset($data)){	 
						$question = $data->question;
						$rates[$data->idx] = array('answer' => $options[$data->idx], 'rates' => $data->rating);
					}
				}
				
				$output = "<h3>$poll_results_label</h3>";
				$output .= "<dl><dt>$question_label</dt><dd>$question</dd>";
				$output .= "<dt>$answers_label</dt>";
				$currentRate = 0;
				$currentRates=array();
				for($i = 0; $i < count($rates); $i++){
					$rate = $rates[$i];
					$rateText = sprintf($answer_structure,$rate['answer'], $rate['rates']);
					$output .= "<dd>$rateText</dd>";
					if($rate['rates']==$rates[$currentRate]['rates']){
						$currentRates[] = $i;
					}
					if($rate['rates']>$rates[$currentRate]['rates']){
						$currentRate = $i;
						$currentRates = array();
					} 
				}
				$output .= "</dd>"; 
				
				$output .= "<dt class=\"most-rated\">$most_rated</dt>";
				foreach($currentRates as $r){
					$rate = $rates[$r];
					$rateText = sprintf($answer_structure,$rate['answer'], $rate['rates']);
					$output .= "<dd class=\"most-rated\">$rateText</dd>";
				}
				$output .="</dl>";
				return $output;
			}
		}

	}	
}
