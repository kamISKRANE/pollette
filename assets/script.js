$(document).ready(function(){
	var rep_textarea = '<textarea rows="6" cols="80" name="answers" id="sp-answers"><?php echo htmlspecialchars($row->answers) ?></textarea><p class="help">Indiquez une réponse par ligne.</p>';
	var rep_checkbox = '<input type="button" value="Ajouter une réponse">';
	$('input[name=type_reponse]').change(function(){
		if($('input[name=type_reponse]:checked').val() == "textarea"){
			$("#reponse").html(rep_textarea);
		}
		else if($('input[name=type_reponse]:checked').val() == "checkbox"){
			$("#reponse").html(rep_checkbox);
		}
		console.log($('input[name=type_reponse]:checked').val());	
	});
});